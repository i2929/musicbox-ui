import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-musicbox-page-not-found',
  templateUrl: './musicbox-page-not-found.component.html',
  styleUrls: ['./musicbox-page-not-found.component.scss']
})
export class MusicboxPageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
