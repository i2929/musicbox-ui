import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicboxPageNotFoundComponent } from './musicbox-page-not-found.component';

describe('MusicboxPageNotFoundComponent', () => {
  let component: MusicboxPageNotFoundComponent;
  let fixture: ComponentFixture<MusicboxPageNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicboxPageNotFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicboxPageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
