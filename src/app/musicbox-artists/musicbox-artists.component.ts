import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Artist } from '../models/artist';
import { MusicboxService } from '../services/musicbox.service';
import { MusicboxArtistsDetailsComponent } from './musicbox-artists-details.component';

@Component({
  selector: 'app-musicbox-artists',
  templateUrl: './musicbox-artists.component.html',
  styleUrls: ['./musicbox-artists.component.scss']
})
export class MusicboxArtistsComponent implements OnInit {

  public artists: Observable<Artist[]>;
  public displayedColumns: string[] = ["name", "actions"];

  constructor(private service: MusicboxService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.artists = this.service.getArtists();
  }

  public editArtist(artist: Artist): void {
    this.dialog.open(MusicboxArtistsDetailsComponent, {
      data: {
        artist: artist,
        readOnly: false
      }
    });
  }

  public viewArtist(artist: Artist): void {
    this.dialog.open(MusicboxArtistsDetailsComponent, {
      data: {
        artist: artist,
        readOnly: true
      }
    });
  }

  public newArtist(): void {
    this.dialog.open(MusicboxArtistsDetailsComponent, {
      data: {
        artist: null,
        readOnly: false
      }
    });
  }

  public openWebPage(webPage: string): void {
    if (!webPage) {
      return;
    }
    console.log("Open new tab: ", webPage);
    window.open(webPage, "__blank");
  }

}
