import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicboxArtistsComponent } from './musicbox-artists.component';

describe('MusicboxArtistsComponent', () => {
  let component: MusicboxArtistsComponent;
  let fixture: ComponentFixture<MusicboxArtistsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicboxArtistsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicboxArtistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
