import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Observable, of } from "rxjs";
import { Album } from "../models/album";
import { Artist } from "../models/artist";
import { ArtistDetails } from "../models/artist-details";
import { MusicboxService } from "../services/musicbox.service";

@Component({
    templateUrl: './musicbox-artists-details.component.html',
    styleUrls: ['./musicbox-artists-details.component.scss']
  })
  export class MusicboxArtistsDetailsComponent implements OnInit {

    public artistNameControl: FormControl;
    public artist: Artist;
    public albums: Observable<Album[]>;

    constructor(@Inject(MAT_DIALOG_DATA) public artistDetails: ArtistDetails,
        private service: MusicboxService) {
    }

    public ngOnInit(){
        this.artist = this.artistDetails.artist;
        this.artistNameControl = new FormControl(this.artist?.name, Validators.required);
        
        if (this.artistDetails.readOnly) {
            this.artistNameControl.disable();
        }
        
        this.albums = this.artist ? this.service.getAlbumsOfArtist(this.artist.id) : of([]);
    }
  }