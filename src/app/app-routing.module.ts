import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MusicboxAlbumComponent } from './musicbox-album/musicbox-album.component';
import { MusicboxArtistsComponent } from './musicbox-artists/musicbox-artists.component';
import { MusicboxPageNotFoundComponent } from './musicbox-page-not-found/musicbox-page-not-found.component';
import { MusicboxSearchComponent } from './musicbox-search/musicbox-search.component';

const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'search', component: MusicboxSearchComponent },
  { path: 'album/:albumId', component: MusicboxAlbumComponent },
  { path: 'artists', component: MusicboxArtistsComponent },
  { path: '**', component: MusicboxPageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
