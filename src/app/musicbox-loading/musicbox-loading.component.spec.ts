import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicboxLoadingComponent } from './musicbox-loading.component';

describe('MusicboxLoadingComponent', () => {
  let component: MusicboxLoadingComponent;
  let fixture: ComponentFixture<MusicboxLoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicboxLoadingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicboxLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
