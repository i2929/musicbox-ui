import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-musicbox-loading',
  templateUrl: './musicbox-loading.component.html',
  styleUrls: ['./musicbox-loading.component.scss']
})
export class MusicboxLoadingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
