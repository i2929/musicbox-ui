import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Album } from '../models/album';
import { Artist } from '../models/artist';
import { Track } from '../models/track';
import { LoadingService } from '../services/loading.service';
import { MusicboxService } from '../services/musicbox.service';

@Component({
  selector: 'app-musicbox-search',
  templateUrl: './musicbox-search.component.html',
  styleUrls: ['./musicbox-search.component.scss']
})
export class MusicboxSearchComponent implements OnInit {

  public artists$: Observable<Artist[]>;
  public artists: Artist[] = [];

  public allAlbums$: Observable<Album[]>;
  public allAlbums: Album[] = [];

  public allTracks$: Observable<Track[]>;
  public allTracks: Track[] = [];

  public albums: Album[] = [];
  public tracks: Track[] = [];

  public artistControl: FormControl;
  public albumControl: FormControl;
  public displayedColumns = ["title", "album", "artist"];

  constructor(private router: Router, 
    private service: MusicboxService,
    private loading: LoadingService) { }

  ngOnInit(): void {
    this.artistControl = new FormControl(null, Validators.required);
    this.albumControl = new FormControl(null);
    this.albumControl.disable();
    
    this.artistControl.valueChanges
      .subscribe(artistId => {
        if (this.albumControl.disabled) {
          this.albumControl.enable();
        }

        this.albumControl.setValue(null);
        this.albums = this.filterAlbum(artistId);
      });

    this.loading.open();

    this.artists$ = this.service.getArtists();
    this.artists$.subscribe(ar => this.artists = ar);

    this.allAlbums$ = this.service.getAlbums();
    this.allAlbums$.subscribe(al => this.allAlbums = al);

    this.allTracks$ = this.service.getTracks().pipe(
      tap(() => this.loading.close()),
      catchError((err) => {
        console.log(err);
        // this.loading.close();
        return of([]);
      })
    );

    this.allTracks$.subscribe(tr => this.allTracks = tr);
  }

  private filterAlbum(artistId: number): Album[] {
    return this.allAlbums.filter(album => album.artistId === artistId);
  }

  public search() {
    if(!this.artistControl.valid || !this.albumControl.valid) {
      console.log("Invalid form");
      return;
    }

    const artist = this.artistControl.value;
    const album = this.albumControl.value;

    const search = {
      artist: artist,
      album: album
    }

    this.allAlbums.forEach(al => al.artist = this.artists.filter(artist => artist.id === al.artistId)[0]);
    this.allTracks.forEach(tr => tr.album = this.allAlbums.filter(al => al.id === tr.albumId)[0]);

    console.log("Searching with: ", search);

    if (album || album === 0) {
      this.tracks = this.allTracks.filter(track => track.albumId === album);
    }
    else {
      let albumIds: number[] = this.allAlbums
        .filter(al => al.artistId === artist)
        .map(al => al.id);
      this.tracks = this.allTracks.filter(tracks => albumIds.includes(tracks.albumId));
    }

    console.log("Search result: ", this.tracks);

  }

  public selectAlbum(track: Track) {
    console.log("Now I should navigate to album/" + track.albumId);
    this.router.navigate(["album/" + track.albumId]);
  }

}
