import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicboxSearchComponent } from './musicbox-search.component';

describe('MusicboxSearchComponent', () => {
  let component: MusicboxSearchComponent;
  let fixture: ComponentFixture<MusicboxSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicboxSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicboxSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
