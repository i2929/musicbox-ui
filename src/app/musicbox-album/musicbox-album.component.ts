import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Album } from '../models/album';
import { Artist } from '../models/artist';
import { Track } from '../models/track';
import { MusicboxService } from '../services/musicbox.service';

@Component({
  selector: 'app-musicbox-album',
  templateUrl: './musicbox-album.component.html',
  styleUrls: ['./musicbox-album.component.scss']
})
export class MusicboxAlbumComponent implements OnInit {

  public albumId: number;
  public album: Album;
  public artist: Artist;
  public tracks: Track[];

  public albumImage: string;

  constructor(private route: ActivatedRoute, private service: MusicboxService) { }

  ngOnInit(): void {
    this.albumId = +this.route.snapshot.paramMap.get("albumId");

    const album$: Observable<Album> = this.service.getAlbum(this.albumId);
    album$.subscribe(al => {
      this.album = al;
      if (al.imageUrl) {
        this.albumImage = al.imageUrl;
      } else {
        this.albumImage = "assets/logo.png";
      }
    });

    this.service.getArtistByAlbumId(this.albumId).subscribe(a => this.artist = a);

    this.service.getTracksByAlbumId(this.albumId).subscribe(t => this.tracks = t);
  }

}
