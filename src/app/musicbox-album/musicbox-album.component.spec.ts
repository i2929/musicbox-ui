import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicboxAlbumComponent } from './musicbox-album.component';

describe('MusicboxAlbumComponent', () => {
  let component: MusicboxAlbumComponent;
  let fixture: ComponentFixture<MusicboxAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicboxAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicboxAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
