export interface Artist {
    id: number;
    name: string;
    webPage?: string;
}