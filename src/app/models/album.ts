import { Artist } from "./artist";

export interface Album {
    id: number;
    title: string;
    artistId: number;
    artist?: Artist;
    imageUrl?: string;
}