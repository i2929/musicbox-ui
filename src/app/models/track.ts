import { Album } from "./album";

export interface Track {
    id: number;
    title: string;
    albumId: number;
    album?: Album;
    duration?: string;
}