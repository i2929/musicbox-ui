import { Artist } from "./artist";

export interface ArtistDetails {
    artist: Artist;
    readOnly: boolean;
}