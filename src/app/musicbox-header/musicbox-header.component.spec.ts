import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicboxHeaderComponent } from './musicbox-header.component';

describe('MusicboxHeaderComponent', () => {
  let component: MusicboxHeaderComponent;
  let fixture: ComponentFixture<MusicboxHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicboxHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicboxHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
