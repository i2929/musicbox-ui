import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-musicbox-header',
  templateUrl: './musicbox-header.component.html',
  styleUrls: ['./musicbox-header.component.scss']
})
export class MusicboxHeaderComponent implements OnInit {

  public title: string = "Welcome to MusicBox";

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public goHome() {
    this.router.navigate([""]);
  }

}
