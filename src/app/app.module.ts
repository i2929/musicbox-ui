import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MusicboxHeaderComponent } from './musicbox-header/musicbox-header.component';
import { MusicboxSearchComponent } from './musicbox-search/musicbox-search.component';

import { MatSelectModule } from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDividerModule} from '@angular/material/divider';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MusicboxAlbumComponent } from './musicbox-album/musicbox-album.component';
import { MusicboxPageNotFoundComponent } from './musicbox-page-not-found/musicbox-page-not-found.component';

import { HttpClientModule } from '@angular/common/http';
import { MusicboxLoadingComponent } from './musicbox-loading/musicbox-loading.component';
import { MusicboxArtistsComponent } from './musicbox-artists/musicbox-artists.component';
import { MusicboxArtistsDetailsComponent } from './musicbox-artists/musicbox-artists-details.component';

@NgModule({
  declarations: [
    AppComponent,
    MusicboxHeaderComponent,
    MusicboxSearchComponent,
    MusicboxAlbumComponent,
    MusicboxPageNotFoundComponent,
    MusicboxLoadingComponent,
    MusicboxArtistsComponent,
    MusicboxArtistsDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MatSelectModule,
    MatButtonModule,
    MatToolbarModule,
    MatDividerModule,
    MatTableModule,
    MatCardModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
