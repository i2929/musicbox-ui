import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../models/config';
import { Observable, of } from 'rxjs';
import { map, switchMap, shareReplay } from 'rxjs/operators';
import { Artist } from '../models/artist';
import { Album } from '../models/album';
import { Track } from '../models/track';

@Injectable({
  providedIn: 'root'
})
export class MusicboxService {

  private beUrl: Observable<string>;

  constructor(private http: HttpClient) {
    this.beUrl = this.http.get<Config>("assets/config.json").pipe(
      map(conf => conf.be_url),
      shareReplay(1)
    );
  }

  public getArtists(): Observable<Artist[]> {
    const url: Observable<string> = this.beUrl;
    return url.pipe(
      switchMap(u => this.http.get<Artist[]>(u + "/artist/all"))
    );
  }

  public getArtist(artistId: number): Observable<Artist> {
    return this.getArtists().pipe(
      map(artists => artists.filter(ar => ar.id === artistId)[0])
    );
  }

  public getAlbums(): Observable<Album[]> {
    const url: Observable<string> = this.beUrl;
    return url.pipe(
      switchMap(u => this.http.get<Album[]>(u + "/album/all"))
    );
  }

  public getAlbum(albumId: number): Observable<Album> {
    const url: Observable<string> = this.beUrl;
    
    //return url.pipe(
    //  switchMap(u => this.http.get<Album>(u + "/albums/" + albumId))
    //);

    return url.pipe(
      switchMap(u => this.getAlbums().pipe(
        map(albums => albums.filter(a => a.id === albumId)[0])
      ))
    );
  }

  public getTracks(): Observable<Track[]> {
    const url: Observable<string> = this.beUrl;
    return url.pipe(
      switchMap(u => this.http.get<Track[]>(u + "/track/all"))
    );
  }

  public getArtistByAlbumId(albumId: number): Observable<Artist> {
    const album: Observable<Album> = this.getAlbum(albumId);
    return album.pipe(
      switchMap(al => this.getArtist(al.artistId))
    );
  }

  public getTracksByAlbumId(albumId: number): Observable<Track[]> {
    return this.getTracks().pipe(
      map(tracks => tracks.filter(tr => tr.albumId === albumId))
    );
  }

  public getAlbumsOfArtist(artistId: number): Observable<Album[]> {
    return this.getAlbums().pipe(
      map(albums => albums.filter(album => album.artistId === artistId))
    );
  }

}
