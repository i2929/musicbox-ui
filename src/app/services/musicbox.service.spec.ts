import { TestBed } from '@angular/core/testing';

import { MusicboxService } from './musicbox.service';

describe('MusicboxService', () => {
  let service: MusicboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MusicboxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
