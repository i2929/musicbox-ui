import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MusicboxLoadingComponent } from '../musicbox-loading/musicbox-loading.component';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private dialogRef: MatDialogRef<any>;

  constructor(public dialog: MatDialog) { }

  public open(): void {
    this.dialogRef = this.dialog.open(MusicboxLoadingComponent, {
      disableClose: true,
      panelClass: "loading-dialog"
    });
  }

  public close(): void {
    setTimeout(() => {
      if (this.dialogRef) {
        this.dialogRef.close();
        this.dialogRef = null;
      }  
    }, 1000);
  }
}
